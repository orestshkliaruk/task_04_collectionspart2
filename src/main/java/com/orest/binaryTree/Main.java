package com.orest.binaryTree;

public class Main {
    public static void main(String[] args) {

        Tree tree = new Tree();
        tree.createTree(13);

        tree.add(12);
        tree.add(56);
        tree.add(58);
        tree.add(59);
        tree.add(62);
        tree.add(2);

        System.out.println(tree.toString(tree.root));
        System.out.println(tree.toString(tree.root.left.left));

        tree.add(6);

        System.out.println(tree.toString(tree.root.left.left));
        System.out.println(tree.toString(tree.root.left.left.right));
        System.out.println(tree.root);

        System.out.println(tree.contains(tree.root, 56));
        System.out.println(tree.contains(tree.root, 57));
        tree.deleteNode(56);
        System.out.println(tree.contains(tree.root, 56));

        TreeGeneric<String> genericTree = new TreeGeneric<>();

        genericTree.insert("AAAA");
        genericTree.insert("bbbb");
        genericTree.insert("sdgfdsgdfg");
        genericTree.insert("AAkekekeAA");
        genericTree.insert("cAAA");

        genericTree.printInorder();
        genericTree.deleteNode("AAkekekeAA");

    }

}
