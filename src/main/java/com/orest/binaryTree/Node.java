package com.orest.binaryTree;

public class Node {
    int value;
    Node left;
    Node right;

    Node(int value) {
        this.value = value;
        right = null;
        left = null;
    }

    public boolean hasChildren() {
        if (this.left == null && this.right == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean hasOneChild() {
        if ((!this.hasLeft() && !this.hasRight()) || (this.hasLeft() && this.hasRight())) {
            return false;
        }

        if (this.hasLeft()) {
            if (this.left.hasChildren()) {
                return false;
            } else {
                return true;
            }
        }
        if (this.hasRight()) {
            if (this.right.hasChildren()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public boolean hasMoreThanOneChildren() {
       if(this.hasChildren() && !this.hasOneChild()){
           return true;
       }else {
           return false;
       }
    }

    private boolean hasLeft() {
        if (this.left == null) {
            return false;
        } else {
            return true;
        }
    }

    private boolean hasRight() {
        if (this.right == null) {
            return false;
        } else {
            return true;
        }
    }

    public Node getTheOnlyChild()throws IllegalArgumentException{
        if(this.hasOneChild()){
            if(this.hasRight()){
                return this.right;
            }else{
                return this.left;
            }
        }else{
            throw new IllegalArgumentException();
        }
    }
}
