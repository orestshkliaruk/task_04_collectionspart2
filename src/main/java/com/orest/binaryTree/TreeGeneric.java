package com.orest.binaryTree;

import java.util.NoSuchElementException;

public class TreeGeneric <T extends Comparable<T>> {

    private NodeGeneric<T> root;

    public void insert(T data) {
        root = insertNode(root, data);
    }

    private NodeGeneric<T> insertNode(NodeGeneric<T> root, T data) {
        if (root == null) {
            return new NodeGeneric<T>(data);
        } else if (data.compareTo(root.data) < 0) {
            root.left = insertNode(root.left, data);
        } else if (data.compareTo(root.data) > 0) {
            root.right = insertNode(root.right, data);
        }
        return root;
    }

    public NodeGeneric<T> deleteNode(T data) {
        System.out.println("Node " + data + " deleted successfully");
        return  this.deleteNodeLogic(root, data);
    }

    private boolean contains(NodeGeneric<T> current, T data) {
        if (current == null) {
            return false;
        }
        if (data == current.data) {
            return true;
        }
        if (data.compareTo(current.data)<0) {
            return contains(current.left, data);
        } else {
            return contains(current.right, data);
        }

    }

    private NodeGeneric<T> deleteNodeLogic(NodeGeneric<T> current, T data) throws NoSuchElementException {
        if (this.contains(current, data)) {

            if (current == null) {
                return current;
            } else if (current.data == data) {
                if (current.hasMoreThanOneChildren()) {
                    T temp = findBiggestValue(current);
                    current.data = temp;
                    System.out.println("The tree was rebuilt. Now deleted node has value: " + current.data);
                    System.out.println("The value of its right child is "+ current.right.data);
                    return current;
                } else if (current.hasOneChild()) {
                    current = current.getTheOnlyChild();
                    return current;
                } else if (!current.hasChildren()) {
                    current = null;
                    return current;
                }
            }
            if (current.data.compareTo(data)>0) {
                current.left = deleteNodeLogic(current.left, current.left.data);
                return current;
            } else {
                current.right = deleteNodeLogic(current.right, current.right.data);
                return current;
            }
        } else {
            throw new NoSuchElementException();
        }
    }

    private T findBiggestValue(NodeGeneric<T> statringNode) {
        if (statringNode.right == null) {
            return statringNode.data;
        } else {
            return findBiggestValue(statringNode.right);
        }
    }

    public void printInorder()
    {
        inorder(root);
        System.out.println();
    }

    private void inorder(NodeGeneric<T> root)
    {
        if (root == null)
            return;

        inorder(root.left);
        System.out.print(" " + root.data);
        inorder(root.right);
    }
}
