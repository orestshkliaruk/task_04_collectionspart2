package com.orest.binaryTree;

import java.util.NoSuchElementException;

public class Tree {
    Node root;


    public Node deleteNode(int value) {
        System.out.println("Node " + value + " deleted successfully");
        return this.deleteNodeLogic(root, value);
    }

    private Node deleteNodeLogic(Node current, int value) throws NoSuchElementException {
        if (this.contains(current, value)) {

            if (current == null) {
                return current;
            } else if (current.value == value) {
                if (current.hasMoreThanOneChildren()) {
                    int temp = findBiggestValue(current);
                    current.value = temp;
                    System.out.println("The tree was rebuilt. Now deleted node had value: " + current.value);
                    System.out.println("The value of its right child is "+ current.right.value);
                    return current;
                } else if (current.hasOneChild()) {
                    current = current.getTheOnlyChild();
                    return current;
                } else if (!current.hasChildren()) {
                    current = null;
                    return current;
                }
            }
            if (current.value < value) {
                current.right = deleteNodeLogic(current.right, current.right.value);
                return current;
            } else {
                current.left = deleteNodeLogic(current.left, current.left.value);
                return current;
            }
        } else {
            throw new NoSuchElementException();
        }
    }

    private Node addNode(int value, Node currentNode) {
        if (currentNode == null) {
            return new Node(value);
        }
        if (currentNode.value > value) {
            currentNode.left = addNode(value, currentNode.left);
        } else if (currentNode.value < value) {
            currentNode.right = addNode(value, currentNode.right);
        } else {
            return currentNode;
        }
        return currentNode;
    }

    public void add(int value) {
        root = addNode(value, root);
    }

    public void createTree(int valueOfRoot) {
        this.add(valueOfRoot);
    }

    public String toString(Node node) {
        if (node.value == this.root.value) {
            return "Tree{" +
                    "Node value =" + node.value + " This is a three root" +
                    '}';
        } else {
            return "Tree{" +
                    "Node value =" + node.value +
                    '}';
        }
    }

    public boolean contains(Node current, int value) {
        if (current == null) {
            return false;
        }
        if (value == current.value) {
            return true;
        }
        if (value < current.value) {
            return contains(current.left, value);
        } else {
            return contains(current.right, value);
        }

    }

    public boolean isEmpty() {
        return root == null;
    }

    private int findBiggestValue(Node statringNode) {
        if (statringNode.right == null) {
            return statringNode.value;
        } else {
            return findBiggestValue(statringNode.right);
        }
    }


}


