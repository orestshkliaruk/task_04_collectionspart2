package com.orest.menu.map;



import java.util.*;

public class Menu {
    boolean exit = false;
    private Map<String, String> menuMap;
    private Map<String, Drawable> mapOfMethods;
    private static Scanner in = new Scanner((System.in));

    public Menu() {
        printWelcome();
        menuMap = new LinkedHashMap<>();
        mapOfMethods = new LinkedHashMap<>();

        menuMap.put("Triangle", "Draw a Triangle");
        menuMap.put("Square", "Draw a Square");
        menuMap.put("Circle", "Draw a Circle");
        menuMap.put("Exit", "Exit");

        mapOfMethods.put("Triangle", this::drawTriangle);
        mapOfMethods.put("Square", this::drawSquare);
        mapOfMethods.put("Circle", this::drawCircle);
        mapOfMethods.put("Exit", this::Exit);

    }

    private void Exit() {
        System.out.println("Thank You, Bye!");
        return;
    }

    private void drawCircle() {
        System.out.println("Drawing Circle");

    }

    private void drawSquare() {
        System.out.println("Drawing Square");
        Drawable drawable = () -> System.out.println(" Drrrraw");


    }

    private void drawTriangle() {
        System.out.println("Drawing Triangle");
    }


    public enum Command {
        TRIANGLE("Triangle"),
        SQUARE("Square"),
        CIRCLE("Circle"),
        EXIT("Exit");

        private final String message;

        Command(String message) {
            this.message = message;
        }

    }

    private void printWelcome() {
        System.out.println("Hello, username");
    }

    private void printMenu() {

        System.out.println("\nChoose what you want to create:");
        /*
        System.out.println("Triangle");
        System.out.println("Square");
        System.out.println("Sircle");
        System.out.println("Exit");
        */
        for (String s : menuMap.values()) {
            System.out.println(s);
        }


    }

    public void showMenu() {
        String choice;
        do {
            printMenu();
            choice = in.nextLine();
            try {
                mapOfMethods.get(choice).draw();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("OOPS, smth went wrong");
            }
        } while (!choice.equals("Exit"));
    }

    private String getInput() {
        String input = null;
        Scanner in = new Scanner(System.in);
        while (input == null) {
            try {
                System.out.println("\nEnter your choice:");
                input = in.nextLine();
            } catch (Exception e) {
                System.out.println("Please enter smth valid");
            }
        }
        return input;
    }

    @FunctionalInterface
    interface Drawable {
        void draw();
    }
}
