package com.orest.menu.map;

import java.util.HashMap;
import java.util.Map;

public class CommandPerformer {
    Map<Menu.Command, Runnable> actions = new HashMap<Menu.Command, Runnable>();

    void run(Menu.Command command)throws IllegalAccessException{
        Runnable action = actions.get(Menu.Command.values());
        if(action==null){
            throw new IllegalArgumentException("You entered non existing command");
        }
        action.run();
    }


}
